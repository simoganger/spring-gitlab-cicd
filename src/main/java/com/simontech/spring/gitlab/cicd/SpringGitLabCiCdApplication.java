package com.simontech.spring.gitlab.cicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGitLabCiCdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringGitLabCiCdApplication.class, args);
	}

}
